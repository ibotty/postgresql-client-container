FROM registry.fedoraproject.org/fedora-minimal:31

LABEL   MAINTAINER=tob@butter.sh \
        io.openshift.tags=backup,restic \
        io.k8s.description="Backup with restic" \
        io.openshift.non-scalable=true

RUN microdnf install postgresql \
 && microdnf clean all

ENTRYPOINT ["/usr/bin/psql"]
